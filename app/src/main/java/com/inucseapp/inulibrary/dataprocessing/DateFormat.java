package com.inucseapp.inulibrary.dataprocessing;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by junha on 2017. 8. 15..
 */

public class DateFormat {

    public String getCurrentTime(){

        return String.valueOf(System.currentTimeMillis());

    }

    public String convertDateForamt(String date){

        Date rawDate = new Date(Long.parseLong(date));
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMdd일 a K:mm", Locale.KOREA);

        return dateFormat.format(rawDate);

    }

}
