package com.inucseapp.inulibrary.dataprocessing;

import android.os.Handler;
import android.os.Message;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.inucseapp.inulibrary.MainActivity.MSG_AFTER_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_BEFORE_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_PARSING_ERROR;

/**
 * Created by junha on 2017. 2. 26..
 */

public class ParsingHtmlThread extends Thread {

    private String result;
    private String url;
    private int position;
    private Handler mHandler;

    public ParsingHtmlThread(){

    }

    public ParsingHtmlThread(Handler handler, String url){
        this.mHandler = handler;
        this.url = url;
    }

    public ParsingHtmlThread(Handler handler, String url, int position){
        this.mHandler = handler;
        this.url = url;
        this.position = position;
    }

    @Override
    public void run() {

        Document document = null;
        StringBuilder html = new StringBuilder();
        String result;

        System.out.println("parsing Thread");

        try{
            //main thread에 보낼 메시지 생성.
            Message msg1 = Message.obtain(mHandler, MSG_BEFORE_PARSING);
            mHandler.sendEmptyMessage(msg1.what);
            msg1.recycle();

            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
                public X509Certificate[] getAcceptedIssuers(){return new X509Certificate[0];}
                public void checkClientTrusted(X509Certificate[] certs, String authType){}
                public void checkServerTrusted(X509Certificate[] certs, String authType){}
            }};

            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            //parsing
            document = Jsoup.connect(url).timeout(60000).get();
            html.append(document.html());

            result = html.toString().replaceAll("[\\n\\r\\t]","").replaceAll(">\\s+", ">").replaceAll("\\s+<","<");

            //parsing 끝
            Message msg2 = Message.obtain(mHandler, MSG_AFTER_PARSING, result);
            mHandler.sendMessage(msg2);

        }catch(Exception e){
            e.printStackTrace();
            //에러가 나면 어떻게 하는가
            Message msg3 = Message.obtain(mHandler, MSG_PARSING_ERROR);
            mHandler.sendEmptyMessage(msg3.what);

        }

    }

    public String DownloadHtml(final String strURL) {

        Document document = null;
        StringBuilder html = new StringBuilder();

        try {
            document = Jsoup.connect(strURL).timeout(60000).get();
            html.append(document.html());

        }catch(Exception e){
            System.out.println("Exception : " + e);
        }

        return html.toString().replaceAll("[\\n\\r\\t]","").replaceAll(">\\s+", ">").replaceAll("\\s+<","<");

    }
}
