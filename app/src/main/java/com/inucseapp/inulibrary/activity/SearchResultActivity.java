package com.inucseapp.inulibrary.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.SearchResultAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.ParsingHtmlThread;
import com.inucseapp.inulibrary.dataprocessing.ProcessingHtmlData;
import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.domain.BookSearchCondition;
import com.inucseapp.inulibrary.domain.SitePage;
import com.inucseapp.inulibrary.tutorial.SearchResultTutorial;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.inucseapp.inulibrary.MainActivity.MSG_AFTER_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_BEFORE_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_PARSING_ERROR;
import static com.inucseapp.inulibrary.MainActivity.SEARCH_URL;

/**
 * Created by junha on 2017. 7. 29..
 */

public class SearchResultActivity extends ToolbarActivity implements SwipyRefreshLayout.OnRefreshListener{

    private SearchResultActivity context;
    private Toolbar toolbar;

    private RecyclerView resultRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SweetAlertDialog sweetAlertDialog;
    private SwipyRefreshLayout swipyRefreshLayout;

    private BookSearchCondition bookSearchCondition;
    private ParsingHtmlThread parsingHtmlThread;
    private SitePage sitePage;

    private SQLHandler sqlHandler;

    private List<Book> bookList;

    private final String REGEX = "<form name=\"briefFrm\" method=\"post\" action=\"\\/search\\/brief\\/service\" onsubmit=\"return checked\\(this\\);\">.*<form name=\"frmSer\"";
    private static String TAG = "searchResult";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initializeView();

        //URL 완성
        BookSearchCondition condition = sqlHandler.selectSearchCondition();

        String query = (String)getIntent().getSerializableExtra("query");
        bookSearchCondition.setKeyword(query);
        bookSearchCondition.setSortMethod(this.convertSortMethod(condition.getSortMethod()));
        bookSearchCondition.setSort(this.convertSort(condition.getSort()));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("'" + query + "' 검색결과");
/*
        if(sqlHandler.selectTutorial("searchresult").get(0) == 0) {

            sqlHandler.updateTutorial("searchresult");

            Intent intent = new Intent(context, SearchResultTutorial.class);
            startActivity(intent);
        }*/


        String URL = SEARCH_URL + bookSearchCondition.toString();

        Log.i(TAG, URL);

        swipyRefreshLayout.setOnRefreshListener(this);

        parsingHtmlThread = new ParsingHtmlThread(mHandler, URL);
        parsingHtmlThread.start();

    }

    public void initializeView (){

        context = SearchResultActivity.this;

        //AD
        MobileAds.initialize(getApplicationContext(),"ca-app-pub-9972298440996794~1848779003");
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5308EA1293651F31192A565607B498E7").build();
        adView.loadAd(adRequest);

        swipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.result_swipy_refresh);

        resultRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        resultRecyclerView.setHasFixedSize(true);
        resultRecyclerView.setLayoutManager(linearLayoutManager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bookList = new ArrayList<>();

        bookSearchCondition = new BookSearchCondition();

        sitePage = new SitePage();

        sqlHandler = new SQLHandler(context);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.toolbar_tutorial:
                Intent intent = new Intent(context, SearchResultTutorial.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        Log.i(TAG, "onRefresh");

        if(sitePage.getBookCurrentPage() < sitePage.getBookTotalPage()) {
            bookSearchCondition.setPageNumber(String.valueOf(sitePage.getBookCurrentPage() + 1));
            String URL = SEARCH_URL + bookSearchCondition.toString();

            parsingHtmlThread = new ParsingHtmlThread(mHandler, URL);
            parsingHtmlThread.start();
        }

        //int viewRange =resultRecyclerView.computeVerticalScrollOffset() + resultRecyclerView.computeVerticalScrollOffset();
        swipyRefreshLayout.setRefreshing(false);


    }

    @Override
    protected void onResume() {

        //Log.i(TAG, "onResume()");
        super.onResume();
    }

    private class MyHandler extends Handler{
        private final WeakReference<SearchResultActivity> mActivity;

        public MyHandler (SearchResultActivity activity){
            mActivity = new WeakReference<SearchResultActivity>(activity);
        }

        public void handleMessage(Message msg){
            //완료 후 실행할 처리 삽입

            switch(msg.what){
                case MSG_BEFORE_PARSING :

                    //Log.i(TAG, "parsing 전");

                    sweetAlertDialog = new SweetAlertDialog(SearchResultActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("데이터 불러오는 중...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();

                    break;
                case MSG_AFTER_PARSING:

                    //message로 html 파싱 결과값을 받아온다.
                    //여기서 ui설정을 한다
//                    Log.i(TAG, "parsing 끝");
                    String parsingResult = msg.obj.toString();

                    ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

                    String splitBookData = processingHtmlData.splitArea(parsingResult, REGEX);

                    String pageInfo = processingHtmlData.splitArea(parsingResult, "<strong>\\d+/\\d+</strong>");

                    List<String> bookHtmlList;

                    //현재 페이지 정보 저장.
                    sitePage = processingHtmlData.getPageInfo(pageInfo);
//                    Log.i(TAG, sitePage.getBookCurrentPage() + "/" + sitePage.getBookTotalPage());

                    bookHtmlList = processingHtmlData.getBookInfoArea(splitBookData);

                    if(bookHtmlList.size()>0) {
                        for (int i = 0; i < bookHtmlList.size(); i++) {
                            Book book = processingHtmlData.getBookInfoFromSearchPage(bookHtmlList.get(i));
                            bookList.add(book);
                        }

                        SearchResultAdapter searchResultAdapter = new SearchResultAdapter(getApplicationContext(), bookList);

                        resultRecyclerView.setAdapter(searchResultAdapter);
                        resultRecyclerView.getLayoutManager().scrollToPosition(bookList.size() - 10);
                    }else{
                        //검색 결과 없음
                        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.no_data_view);
                        ((TextView)viewGroup.findViewById(R.id.no_data_text)).setText("검색 결과가 없습니다.");
                        viewGroup.setVisibility(View.VISIBLE);
                    }
                    sweetAlertDialog.dismiss();

                    break;

                case MSG_PARSING_ERROR:

                    //실패원인을 찾으면 좋을 것 같음
//                    Log.i(TAG, "parsing 에러");

                    sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    sweetAlertDialog.setTitleText("불러오기 실패");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            sweetAlertDialog = null;

                            finish();
                        }
                    });
                    //parsing에러가 났을 경우
                    break;

                default:
            }

        }

    }

    private final MyHandler mHandler= new MyHandler(this);

    public String convertSortMethod(String option){

        switch(option){
            case "서명" :
                return "DISP01";

            case "저자" :
                return "DISP02";

            case "출판사" :
                return "DISP03";

            case "청구기호" :
                return "DISP04";

            case "발행년" :
                return "DISP06";

            default :
                return "DISP01";
        }

    }

    public String convertSort(String option){

        switch(option){
            case "오름차순" :
                return "ASC";

            case "내림차순" :
                return "DESC";

            default :
                return "ASC";
        }
    }
}
