package com.inucseapp.inulibrary.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.inucseapp.inulibrary.MainActivity;
import com.inucseapp.inulibrary.R;

import java.util.List;

/**
 * Created by junha on 2017. 8. 24..
 */

public class ToolbarActivity extends AppCompatActivity  {

    private static final String TAG = "ToolbarActivity";

    private MenuItem item;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;
        String className = null;

        //Log.i(TAG,String.valueOf(item.getItemId()));

        switch(item.getItemId()){
            case R.id.toolbar_search :

                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);

                break;

            case R.id.toolbar_bookmark :

                className = this.getRunActivity();

                if(!"com.inucseapp.inulibrary.activity.BookmarkActivity".equals(className)){
                    intent = new Intent(this, BookmarkActivity.class);
                    startActivity(intent);
                }

                break;

            case R.id.toolbar_info :

                //Toast.makeText(this, "toolbar_info", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, DeveloperActivity.class);
                startActivity(intent);

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getRunActivity(){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = activityManager.getRunningTasks(10);

        return taskInfo.get(0).topActivity.getClassName();
    }
}
