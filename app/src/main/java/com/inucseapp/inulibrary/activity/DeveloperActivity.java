package com.inucseapp.inulibrary.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.inucseapp.inulibrary.BuildConfig;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 14..
 */

public class DeveloperActivity extends ToolbarActivity {

    private Context context;
    private int versionCode;
    private String versionName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_develop_info);

        versionCode = BuildConfig.VERSION_CODE;
        versionName = BuildConfig.VERSION_NAME;

        ImageView imageView = (ImageView)findViewById(R.id.imageView2);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),versionCode + "/" + versionName,Toast.LENGTH_SHORT ).show();
            }
        });

    }
}
