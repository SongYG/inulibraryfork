package com.inucseapp.inulibrary.activity;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.inucseapp.inulibrary.MainActivity;
import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.tutorial.HistoryTutorial;


/**
 * Created by junha on 2017. 7. 29..
 */

public class SearchToolbarActivity extends AppCompatActivity  implements SearchView.OnQueryTextListener,
                                                                   MenuItemCompat.OnActionExpandListener,
                                                                   View.OnKeyListener{

    private static final String TAG = "SearchToolbarActivity";

    private SearchView searchView;

    private MenuItem searchItem;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        SQLHandler sqlHandler = new SQLHandler(getApplicationContext());

        if(sqlHandler.selectHistoryTitle(query) == null){
            sqlHandler.insertHistory(query);
        }else{
            sqlHandler.updateHistory(query);
        }

        Intent intent = new Intent(this, SearchResultActivity.class);
        intent.putExtra("query", query);

        startActivity(intent);

        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) { return false; }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (keyCode == event.KEYCODE_ENTER){
            searchView.onActionViewCollapsed();
            return true;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;

        switch(item.getItemId()){
            case R.id.action_bookmark :

                intent = new Intent(this, BookmarkActivity.class);
                startActivity(intent);
                break;

            case R.id.action_tutorial :

                intent = new Intent(this, HistoryTutorial.class);
                startActivity(intent);

                break;

            case R.id.action_info :

                intent = new Intent(this, DeveloperActivity.class);
                startActivity(intent);

                break;

        }


        return super.onOptionsItemSelected(item);
    }
}
