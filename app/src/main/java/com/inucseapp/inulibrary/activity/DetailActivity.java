package com.inucseapp.inulibrary.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.DetailAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.dataprocessing.ParsingHtmlThread;
import com.inucseapp.inulibrary.dataprocessing.ProcessingHtmlData;
import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.tutorial.BookmarkTutorial;
import com.inucseapp.inulibrary.tutorial.DetailTutorial;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.inucseapp.inulibrary.MainActivity.MSG_AFTER_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_BEFORE_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_PARSING_ERROR;

/**
 * Created by syg11 on 2017-07-06.
 */

public class DetailActivity extends ToolbarActivity implements View.OnClickListener{

    private Context context;

    private Toolbar toolbar;

    private ListView detailListView;
    private DetailAdapter detailAdapter;

    private TextView detailTitle;
    private TextView detailAuthor;
    private TextView detailPublisher;
    private TextView detailIssueYear;

    private FloatingActionButton bookmarkButton;

    private RecyclerView detailRecyclerView;
    private SweetAlertDialog sweetAlertDialog;
    private LinearLayoutManager linearLayoutManager;

    private SQLHandler sqlHandler;

    private Book bookInfo;
    private List<Book> bookDetailInfoList;

    private static final String DETAIL_URL = "http://lib.inu.ac.kr/search/detail/";
    private static final String REGEX = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><script";

    private static final String TAG = "DetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initializeView();

        bookInfo = (Book)getIntent().getSerializableExtra("bookinfo");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("상세정보");

        detailTitle.setText(bookInfo.getBookTitle());
        detailAuthor.setText(bookInfo.getAuthor());
        detailPublisher.setText(bookInfo.getPublisher());
        detailIssueYear.setText(bookInfo.getIssueYear());
/*
        if(sqlHandler.selectTutorial("detail").get(0) == 0) {

            sqlHandler.updateTutorial("detail");

            Intent intent = new Intent(context, DetailTutorial.class);
            startActivity(intent);
        }*/


        String URL = DETAIL_URL + bookInfo.getBookUrl();

        //Log.i(TAG, URL);

        ParsingHtmlThread parsingHtmlThread = new ParsingHtmlThread(handler, URL);
        parsingHtmlThread.start();
    }

    private void initializeView(){

        context = getApplicationContext();

        //AD
        MobileAds.initialize(getApplicationContext(),"ca-app-pub-9972298440996794~1848779003");
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5308EA1293651F31192A565607B498E7").build();
        adView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bookmarkButton = (FloatingActionButton) findViewById(R.id.detail_setbookmark);
        bookmarkButton.setOnClickListener(this);

        //textView
        detailTitle = (TextView) findViewById(R.id.detail_title);
        detailAuthor = (TextView) findViewById(R.id.detail_author);
        detailPublisher = (TextView) findViewById(R.id.detail_publisher);
        detailIssueYear = (TextView) findViewById(R.id.detail_issueyear);

        detailRecyclerView = (RecyclerView) findViewById(R.id.detail_recycle);
        linearLayoutManager = new LinearLayoutManager(context);
        detailRecyclerView.setHasFixedSize(true);
        detailRecyclerView.setLayoutManager(linearLayoutManager);

        //dbhandler
        sqlHandler = new SQLHandler(context);

        bookInfo = new Book();
        bookDetailInfoList = new ArrayList<>();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.toolbar_tutorial:

                Intent intent = new Intent(context, DetailTutorial.class);
                startActivity(intent);

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {

            switch(msg.what){
                case MSG_BEFORE_PARSING :

                    //Log.i(TAG, "parsing 전" );

                    sweetAlertDialog = new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("데이터 불러오는 중...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();

                    break;

                case MSG_AFTER_PARSING :

                    //Log.i(TAG, "parsing 끝");
                    String parsingResult = msg.obj.toString();
                    ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
                    String splitArea = processingHtmlData.splitArea(parsingResult, REGEX);
                    List<String> detailHtmlList = processingHtmlData.getBookDetailInfo(splitArea);

                    //대출 가능 도서 목록
                    if(detailHtmlList != null){
                        try {
                            for(int i=0; i<detailHtmlList.size(); i++){
                                Book book = processingHtmlData.getBookInfoFromDetailPage(detailHtmlList.get(i));
                                bookDetailInfoList.add(book);
                            }
                        }catch (Exception e){
                            Log.e("Exception",e.getMessage());
                        }

                    }else{
                        detailRecyclerView.setAdapter(new DetailAdapter(context, bookDetailInfoList, R.layout.activity_detail));
                        sweetAlertDialog.dismiss();
                        break;
                    }
                    Book locationInfo = processingHtmlData.distinguishLocation(bookDetailInfoList);

                    bookInfo.setBookTotalNum(String.valueOf(bookDetailInfoList.size()));
                    bookInfo.setBookBorrowableHaksan(locationInfo.getBookBorrowableHaksan());
                    bookInfo.setBookBorrowableEducation(locationInfo.getBookBorrowableEducation());
                    bookInfo.setBookBorrowableSonas(locationInfo.getBookBorrowableSonas());

                    if(bookDetailInfoList.size() != 0) {
                        bookInfo.setBookRegisterID(bookDetailInfoList.get(0).getBookRegisterID());
                    }

                    DateFormat dateFormat = new DateFormat();
                    bookInfo.setSearchDate(dateFormat.getCurrentTime());
                    detailRecyclerView.setAdapter(new DetailAdapter(context, bookDetailInfoList, R.layout.activity_detail));

                    sweetAlertDialog.dismiss();


                    break;

                case MSG_PARSING_ERROR :

                    Log.i(TAG, "parsing ERROR");

                    sweetAlertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    sweetAlertDialog.setTitleText("불러오기 실패");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            sweetAlertDialog = null;

                            finish();
                        }
                    });

                    break;

                default:
            }

        }
    };

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.detail_setbookmark:
                if(bookInfo.getBookRegisterID() != null){

                    boolean isExist = sqlHandler.select_bookid(bookInfo.getBookID());
                    if(!isExist){
                        Snackbar.make(v, bookInfo.getBookTitle() + "(을)를 등록했습니다.", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                        sqlHandler.insert_bookinfo(bookInfo);
                    }else{
                        Snackbar.make(v, "이미 등록된 도서입니다.", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                    }

                }else{
                    Snackbar.make(v, "등록 할 수 없는 도서 입니다.", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                }
                break;
        }
    }
}
