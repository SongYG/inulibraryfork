package com.inucseapp.inulibrary.domain;

/**
 * Created by junha on 2017. 7. 29..
 */

public class SitePage {

    //현재 페이지
    private int bookCurrentPage;        //현재 페이지
    private int bookTotalPage;          //총 페이지 수


    public int getBookCurrentPage() {
        return bookCurrentPage;
    }

    public void setBookCurrentPage(int bookCurrentPage) {
        this.bookCurrentPage = bookCurrentPage;
    }

    public int getBookTotalPage() {
        return bookTotalPage;
    }

    public void setBookTotalPage(int bookTotalPage) {
        this.bookTotalPage = bookTotalPage;
    }


}
