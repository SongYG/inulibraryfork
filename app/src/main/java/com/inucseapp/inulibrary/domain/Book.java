package com.inucseapp.inulibrary.domain;

import java.io.Serializable;

/**
 * Created by junha on 2017. 2. 27..
 */

public class Book implements Serializable{

    private String index;

    //검색페이지에서 검색한 후 나온 결과
    private String bookTitle;           //서명
    private String author;              //저자
    private String publisher;           //출판사
    private String bookID;              //청구기호
    private String issueYear;           //발행년
    private String bookUrl;             //책페이지 Url

    //책을 자세히 조회한 결과
    private String bookType;            //자료유형
    private String BookRegisterID;  //책 등록 번호
    private String bookLocation;        //소장처
    private String bookReturnDate;      //반납예정일
    private String bookState;           //도서상태
    private String bookTotalNum;        //총 권수
    //private String bookBorrowNum;       //대여 수
    private String bookBorrowableHaksan;
    private String bookBorrowableEducation;
    private String bookBorrowableSonas;

    private String searchDate;

    public String getBookTotalNum() {
        return bookTotalNum;
    }

    public void setBookTotalNum(String bookTotalNum) {
        this.bookTotalNum = bookTotalNum;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(String issueYear) {
        this.issueYear = issueYear;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getBookLocation() {
        return bookLocation;
    }

    public void setHoldingInstitution(String bookLocation) {
        this.bookLocation = bookLocation;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setBookLocation(String bookLocation) {
        this.bookLocation = bookLocation;
    }

    public String getBookReturnDate() {
        return bookReturnDate;
    }

    public void setBookReturnDate(String bookReturnDate) {
        this.bookReturnDate = bookReturnDate;
    }

    public String getBookState() {
        return bookState;
    }

    public void setBookState(String bookState) {
        this.bookState = bookState;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

    public String getBookRegisterID() {
        return BookRegisterID;
    }

    public void setBookRegisterID(String bookRegisterID) {
        BookRegisterID = bookRegisterID;
    }

    public String getBookBorrowableHaksan() {
        return bookBorrowableHaksan;
    }

    public void setBookBorrowableHaksan(String bookBorrowableHaksan) {
        this.bookBorrowableHaksan = bookBorrowableHaksan;
    }

    public String getBookBorrowableEducation() {
        return bookBorrowableEducation;
    }

    public void setBookBorrowableEducation(String bookBorrowableEducation) {
        this.bookBorrowableEducation = bookBorrowableEducation;
    }

    public String getBookBorrowableSonas() {
        return bookBorrowableSonas;
    }

    public void setBookBorrowableSonas(String bookBorrowableSonas) {
        this.bookBorrowableSonas = bookBorrowableSonas;
    }



//    @Override
//    public String toString(){
//        return  this.getBookTitle() + " , " +
//                this.getAuthor() + " , " +
//                this.getPublisher() + " , " +
//                this.getBookID() + " , " +
//                this.getIssueYear() + " , " +
//                this.getBookUrl();
//    }
}
