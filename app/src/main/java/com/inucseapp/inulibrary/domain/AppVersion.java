package com.inucseapp.inulibrary.domain;

/**
 * Created by syg11 on 2017-09-02.
 */

public class AppVersion {
    private long lastest_version_code;
    private String message;

    private AppVersion(){}

    public AppVersion(long lastest_version_code, String message){
        this.lastest_version_code = lastest_version_code;
        this.message = message;
    }


    public long getLastest_version_code(){
        return lastest_version_code;
    }

    public void setLastest_version_code(long lastest_version_code) {
        this.lastest_version_code = lastest_version_code;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }
}
