package com.inucseapp.inulibrary.domain;

/**
 * Created by syg11 on 2017-07-06.
 */

public class SearchHistory {

    private int index;
    private String searchWord;
    private String searchDate;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSearchWord(){
        return this.searchWord;
    }

    public void setSearchWord(String searchWord){
        this.searchWord = searchWord;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }

}
