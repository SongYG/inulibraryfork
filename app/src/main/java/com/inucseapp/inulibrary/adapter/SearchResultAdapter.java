package com.inucseapp.inulibrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.activity.DetailActivity;
import com.inucseapp.inulibrary.domain.Book;

import java.util.List;

/**
 * Created by syg11 on 2017-07-06.
 * Modified by Junha on 2017-08-05.
 *   ListView -> CardView
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    Context context;
    List<Book> bookList;

    public SearchResultAdapter(Context context, List<Book> bookList){
        this.context = context;
        this.bookList = bookList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_result, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Book book = bookList.get(position);

        holder.bookTitleView.setText(book.getBookTitle());
        if("".equals(book.getBookID())){
            holder.bookIDView.setVisibility(View.GONE);
        }else {
            holder.bookIDView.setText(book.getBookID());
        }
        holder.bookAuthorView.setText(book.getAuthor());
        holder.bookPublisherView.setText(book.getPublisher());
        holder.bookIssueYearView.setText(book.getIssueYear());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //여기서 상세페이지로 넘어가야함.
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("bookinfo", book);

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.bookList.size();
    }

    //inner class

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView bookTitleView;
        TextView bookIDView;
        TextView bookAuthorView;
        TextView bookPublisherView;
        TextView bookIssueYearView;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            bookTitleView = (TextView) itemView.findViewById(R.id.bookTitleResult);
            bookIDView = (TextView) itemView.findViewById(R.id.bookIDResult);
            bookAuthorView = (TextView) itemView.findViewById(R.id.bookAuthorResult);
            bookPublisherView = (TextView) itemView.findViewById(R.id.bookPublisherResult);
            bookIssueYearView = (TextView) itemView.findViewById(R.id.bookIssueYearResult);
            cardView = (CardView) itemView.findViewById(R.id.result_cardview);

        }
    }

}
