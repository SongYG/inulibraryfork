package com.inucseapp.inulibrary.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by junha on 2017. 7. 24..
 */

public class SqliteOpenHelper extends SQLiteOpenHelper {

    public SqliteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql;

        sql = "create table searchhistory" +
                        "(idx integer primary key autoincrement," +
                        "word text," +
                        "date text);";

        db.execSQL(sql);

        //index, 서명, 저자, 출판사, 청구기호, 발행년도, 책URL, 책등록번호, 소장처, 총권수, 대여수, 즐겨찾기등록일.
       sql = "create table bookinfo" +
                    "(idx integer primary key autoincrement," +
                    "title text," +
                    "author text," +
                    "publisher text," +
                    "id text," +
                    "issue text," +
                    "url text," +
                    "registerid text," +
                    "haksan text," +
                    "sonas text," +
                    "education text," +
                    "totalbooks text," +
                    "borrowablebooks text," +
                    "bmregister text);";

        db.execSQL(sql);

        sql = "create table searchcondition" +
                    "(idx integer primary key," +
                    "search text," +
                    "sort text);";

        db.execSQL(sql);

        sql = "create table tutorial" +
                "(idx integer primary key," +
                "appmain integer," +
                "bookmark integer," +
                "history integer," +
                "searchresult integer," +
                "detail integer);";

        db.execSQL(sql);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "drop table bookinfo;";
        db.execSQL(sql);
        onCreate(db);

    }
}
