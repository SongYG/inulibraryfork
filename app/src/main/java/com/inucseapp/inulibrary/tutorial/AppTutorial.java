package com.inucseapp.inulibrary.tutorial;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 6..
 */

public class AppTutorial extends AppIntro {

    int backgroundColor = 0xFF3F51B5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("안녕하세요!");
        sliderPage1.setDescription("인천대학교 도서검색기 입니다.");
        sliderPage1.setImageDrawable(R.drawable.tutorial_main_01);
        sliderPage1.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("도서검색");
        sliderPage2.setDescription("도서관 소장자료를 쉽게 검색할 수 있습니다.");
        sliderPage2.setImageDrawable(R.drawable.tutorial_main_02);
        sliderPage2.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage2));

        SliderPage sliderPage3 = new SliderPage();
        sliderPage3.setTitle("상세보기");
        sliderPage3.setDescription("책을 대여할 수 있는지 쉽게 확인할 수 있습니다.");
        sliderPage3.setImageDrawable(R.drawable.tutorial_main_03);
        sliderPage3.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage3));

        SliderPage sliderPage4 = new SliderPage();
        sliderPage4.setTitle("관심도서등록");
        sliderPage4.setDescription("관심있는 도서를 등록해 청구기호와 대출가능 여부를 쉽게 확인할 수 있습니다.");
        sliderPage4.setImageDrawable(R.drawable.tutorial_main_04);
        sliderPage4.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage4));

        SliderPage sliderPage5 = new SliderPage();
        sliderPage5.setTitle("사용법 다시보기");
        sliderPage5.setDescription("원하는 페이지에서 '페이지 설명'을 클릭하시면 사용방법을 보실 수 있습니다.");
        sliderPage5.setImageDrawable(R.drawable.tutorial_main_05);
        sliderPage5.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage5));

        SliderPage sliderPage6 = new SliderPage();
        sliderPage6.setTitle("지금 바로 이용해보세요!");
        sliderPage6.setDescription("'인천대 도서검색기'는 학교 공식 앱이 아닙니다.");
        sliderPage6.setImageDrawable(R.drawable.tutorial_main_01);
        sliderPage6.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage6));


    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
