package com.inucseapp.inulibrary.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 6..
 */

public class DetailTutorial extends AppIntro{

    int backgroundColor = 0xFF3F51B5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("선택한 도서의 상세정보를 볼 수 있습니다.");
        sliderPage1.setDescription("대출가능여부, 청구기호, 도서위치와 반납예정일을 확인할 수 있습니다.");
        sliderPage1.setImageDrawable(R.drawable.tutorial_detail_01);
        sliderPage1.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("북마크에 등록해보세요!");
        sliderPage2.setDescription("관심도서 목록을 한눈에 확인할 수 있습니다.");
        sliderPage2.setImageDrawable(R.drawable.tutorial_detail_02);
        sliderPage2.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage2));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
